// Wagmi Bot, Version 0.0.1 2021-01-09 (full english by GANTZ)

// Add bot to discord server:
// https://discord.com/oauth2/authorize?client_id=795347075000434718&scope=bot&permissions=8
// Tuto: https://www.digitaltrends.com/gaming/how-to-make-a-discord-bot/
// Tuto: https://medium.com/davao-js/tutorial-creating-a-simple-discord-bot-9465a2764dc0
// node index.js

//Send tx without having to unlock the account 
//https://stackoverflow.com/questions/46611117/how-to-authenticate-and-send-contract-method-using-web3-js-1-0

// Get ERC20 Token Balance
// https://medium.com/@piyopiyo/how-to-get-erc20-token-balance-with-web3-js-206df52f2561
// https://piyolab.github.io/playground/ethereum/getERC20TokenBalance/

// Send Send ERC20 Token
// https://piyolab.github.io/playground/ethereum/sendERC20Token/

const Web3          = require('web3');
const Discord       = require('discord.js'); //https://github.com/discordjs/discord.js
//const BigNumber     = require('bignumber.js');
const fs            = require("fs");
const botSettings   = require('./botSettings.json');
const { resourceUsage } = require('process');

const minABI = [
    // balanceOf
    {
        "constant":true,
        "inputs":[{"name":"_owner","type":"address"}],
        "name":"balanceOf",
        "outputs":[{"name":"balance","type":"uint256"}],
        "type":"function"
    },
    // decimals
    {
        "constant":true,
        "inputs":[],
        "name":"decimals",
        "outputs":[{"name":"","type":"uint8"}],
        "type":"function"
}];

const minABI2 = [
    // transfer
    {
      "constant": false,
      "inputs": [
        {
          "name": "_to",
          "type": "address"
        },
        {
          "name": "_value",
          "type": "uint256"
        }
      ],
      "name": "transfer",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "type": "function"
    }
];

// Initialize web3
const web3 = new Web3(Web3.givenProvider || botSettings.web3provider);

// Initialize Discord Bot
const bot = new Discord.Client({disableEveryone:true});

bot.on('ready', () => {
    console.log(`Wagmi Bot Initializing, ${bot.user.tag}`);
});

//bot.on('message', function (user, userID, channelID, message, evt) {
bot.on('message',async message => {

    //const user = message.author.name; //legacy
    const serverId = message.guild.id;
    const userName = message.author.username;
    const userID = message.author.id;
    const channelID = message.channel.id;
    
    if(message.channel.type === "dm") return;

    if( 
        channelID === '813450473260580904' || // EXPANSE Tokens #PRM
        channelID === '424253832160083996' || // EXPANSE #bot-commands
        channelID === '375595824370941953' || // EXPANSE #spanish-language
        channelID === '780959976695922719' || // EXPANSE #steak
        channelID === '825362595288055838' || // EXPANSE #alym
        channelID === '614616294897221652' || // PRM test
        channelID === '527521242769653760' || // PRM -bots
        channelID === '527515942251659266' || // PRM -general
        channelID === '527545622006464512' || // PRM -sorteos
        channelID === '527540917083832320' || // PRM -rains
        channelID === '789589426794659910' || // PRM hy511
        channelID === '797512250893664256' || // PRM svit
        channelID === '537789774593785858' || // PRM-Testing general
        channelID === '538132696489000980'  // PRM-Testing bot-test
        ){ 
		//do nothing and continue with bot command...
	}else{
		//return message.channel.send("Please write commands on the BOTS channel, thanks");
		return;
    }
    
    //Get arguments
	const args = message.content.split(' ');

    //Get current prefix
    const prefix = message.content.split('.'); //console.log("Prefix Actual: "+prefix[0]);
    const botPrefix = prefix[0].toLowerCase()+"."; //asign dinamic prefix
    let tokenKeyName = prefix[0].toUpperCase();
    //console.log("Token Name:", tokenKeyName);

    if( botSettings.tokens[prefix[0].toUpperCase()] === undefined){ //Validate if token name/prefix exist
        return;
    }

    /*
    for (var key in botSettings.tokens) {
        if(key == tokenName){
            console.log(key);
            console.log(botSettings.tokens[key]);
        }
    }*/

//-- Debug
    if(message.content.toLowerCase().startsWith(botPrefix+"debug")){
        //console.log("Tip:" + userID);
        //console.log("Debug: ", args[1]);
        //console.log("Debug: ", args[1]); //print: <@!300412335275769856>
        //console.log("tag",message.author.tag);
        //let user_id = args[1].replace(/\D/g, ""); //extract ID user
        //return message.channel.send( user_id );
        //return message.channel.send( `<@${message.author.id}>` );

        //console.log("Debug: ", botSettings.tokens[prefix[0].toUpperCase()] );

        //console.log("MSG ID: ", message.guild.id);
        
        const guild = bot.guilds.cache.get(serverId);
        if (!guild) return console.log("Couldn't get the guild.");

        const members = guild.members.cache.filter(member => member.roles.cache.find(role => role.name === "arole")).map(member => member.id);
        //const members = guild.members.cache.filter(member => member.roles.cache.find(role => role.name === "arole")).map(member => member.id);
        //const members = guild.members.cache.map(member => member.id);

        //console.log("DEBUG: ",bot.guilds.cache);
        console.log("DEBUG: ",members);
        
    }


//--------------------------
// COMMANDS FOR USER
//--------------------------


//-- Register user by discord ID
    if(message.content.toLowerCase().startsWith(botPrefix+"register ")){
        const address = args[1];
        let msg = "";

        if(web3.utils.isAddress(args[1])){	
            let data = getJson(botSettings.usersById);
            //console.log("add: "+ Object.values(data).includes(address));
            //console.log("userID: "+ Object.keys(data).includes(userID));
            if(!Object.values(data).includes(address) && !Object.keys(data).includes(userID)){		
                data[userID] = address;
                msg = `:tada: <@!${userID}> Has been registered successfully ur EXP address: ${address}`;
                
                fs.writeFile(botSettings.usersById, JSON.stringify(data), (err) => {
                    if (err) throw err;
                        console.log( `A new user has registered your wallet: ${userName}` ); //The file has been saved.
                    });	
                
            } else {
                msg = `:thumbsup: <@!${userID}> U have already registered your wallet, to update it use the command **${botPrefix}changeadd**`;
            }
        } else {
            msg = `:open_mouth: <@!${userID}> U trying to register an invalid EXP address. Please try again and make sure it has the following format: **${botPrefix}register 0xAddress...**`;
        }
        
        return message.channel.send(msg);            
    }


//-- Change registration with bot.
    if(message.content.toLowerCase().startsWith(botPrefix+"changeadd ")){
        const address = args[1];
        let msg = "";
        
        if(web3.utils.isAddress(args[1])){
            let data = getJson(botSettings.usersById);
            if(Object.keys(data).includes(userID)){
                if(address != data[userID]){
                    data[userID] = address;
                    fs.writeFile(botSettings.usersById, JSON.stringify(data), (err) => {
                        if (err) throw err;
                            console.log(`User ${userName} wallet has been changed.`); //The file has been changed
                        });
                    msg = `:thumbsup: <@!${userID}> Your old EXP address has been changed to: **${address}**`;
                } else {
                    msg = `<@!${userID}> Seems that you already have this address registered, use a different one if you are trying to change the previous one.`;
                }
            } else {
                msg = `:open_mouth: You are not in the list, first register your EXP address with the command **${botPrefix}register** *<address>*`; 
            }
        } else {
            msg = `:open_mouth: <@!${userID}> you are trying to register a wrong EXP address, try again with the format: **${botPrefix}register 0xAddress**`;
        }

        return message.channel.send(msg);
    }

//-- Check to see user wallet.
    if(message.content.toLowerCase() == botPrefix+"myaddress"){
        const data = getJson(botSettings.usersById);
        const wallet = data[userID];
        let msg = "";

        if(Object.keys(data).includes(userID)){
            msg = `:pouch: <@!${userID}> your EXP token wallet is: **${wallet}**`; //already registered
        } else {
            msg = `:open_mouth: <@!${userID} you are not registered, use the command **${botPrefix}register** *<address>*`; //Plese register
        }

        return message.channel.send(msg);
    }

//-- Get token balance
    if(message.content.toLowerCase() === botPrefix+"bal" || 
       message.content.toLowerCase() === botPrefix+"balance"){

        //Get current token info
        const tokenInfo = botSettings.tokens[tokenKeyName];
        //console.log("Token Info:", tokenInfo);
        //console.log("Obj: ",tokenInfo);

        const data = getJson(botSettings.usersById);
        const walletAddress = data[userID];

        if(tokenInfo === undefined){
            return message.channel.send("Sorry, the token or currency is not available in our bot yet.");
        }

        if(walletAddress === undefined){
            return message.channel.send("seems that you have not yet registered your wallet.");
        }

        //Get Token balance            
        //Get ERC20 Token contract instance
        //const contract = new web3.eth.Contract(minABI, tokenAddress, {from:walletAddress});
        const MyContract = new web3.eth.Contract(minABI, tokenInfo.address);

        // Call balanceOf function
        MyContract.methods.balanceOf(walletAddress).call()
        .then(function(balance){
            //console.log( "Total PRM:", (balance/Math.pow(10,18)).toFixed(3) );
            
            balance = (balance/Math.pow(10,tokenInfo.decimals)).toFixed(8);
            
            message.channel.send( `:moneybag: <@!${userID}> tienes **${balance} ${botPrefix.toUpperCase()}**, you can see your complete balances in **www.expexplorer.guarda.co**` );    
        });            

    }


//-- Send Tip a user.
    if(message.content.toLowerCase().startsWith(botPrefix+"tip ")){
        
        if( userID=="370711244245565441" || //Anabel
            userID=="339843008323256322" || //Ruul 
            userID=="300412335275769856" || //Bitjohn
            userID=="351489283166699520" || //Omar
            userID=="419467013728108544" || //Mello
            userID=="547477878804054028" || //JJJH82
            userID=="360276639147360266" || //madwillysg
            userID=="537600835966861314" || //Wedergarten ALYM
            userID=="746169184135675947" || //Stoice ALYM
            userID=="778048794625179690" || //Stefal EABN
            userID=="804032424215969822" || //Just a Seal x420
            userID=="394268010929455104" //Aseriousgogetta
            ){ 
                let msg = "";
                let user_id = args[1].replace(/\D/g, ""); //Extract Id of user
                let amount = Number(args[2]);

                //Get current token info
                let tokenInfo = botSettings.tokens[tokenKeyName];
                //console.log("Token Info:", tokenInfo);
                
                if(tokenInfo === undefined){
                    return message.channel.send("Sorry, the token or currency is not available in our bot yet.");
                }

                if (!amount){ // if use wrong amount (string or something)
                    return message.channel.send(`:thinking: You have entered an invalid amount of ${tokenKeyName}, Try again.`);
                }

                //Token tips rights for user Just a Seal
                if( userID=="804032424215969822" && tokenKeyName.toUpperCase() != "X420" ){
                    return message.channel.send("Sorry, you are not authorized to send tips for this token.");
                }

                //Token tips rights for user token a Stefal
                if( userID=="778048794625179690"){
                    if(tokenKeyName.toUpperCase() == "EABN" || tokenKeyName.toUpperCase() == "EVLTZ"){
                        //let's go
                    }else{
                        return message.channel.send("Sorry, you are not authorized to send tips for this token.");
                    }
                }

                //Token tips rights for user Stoice
                if( userID=="746169184135675947" && tokenKeyName.toUpperCase() != "ALYM" ){
                    return message.channel.send("Sorry, you are not authorized to send tips for this token.");
                }

                //Token tips rights for user Wedergarten
                if( userID=="537600835966861314" && tokenKeyName.toUpperCase() != "ALYM" ){
                    return message.channel.send("Sorry, you are not authorized to send tips for this token.");
                }
              
                //Token tips rights for user Omar
                if( userID=="351489283166699520" && tokenKeyName.toUpperCase() != "STK" ){
                    return message.channel.send("Sorry, you are not authorized to send tips for this token.");
                }

                //Token tips rights for user Mello
                if( userID=="419467013728108544" && tokenKeyName.toUpperCase() != "HY511" ){
                    return message.channel.send("Sorry, you are not authorized to send tips for this token.");
                }

                //Token tips rights for user JJJH82
                if( userID=="547477878804054028" && tokenKeyName.toUpperCase() != "SVIT" ){
                    return message.channel.send("Sorry, you are not authorized to send tips for this token.");
                }

                //Token tips rights for user madwillysg
                if( userID=="360276639147360266" && tokenKeyName.toUpperCase() != "NST" ){
                    return message.channel.send("Sorry, you are not authorized to send tips for this token.");
                }
                
                //Token tips rights for user madwillysg
                if( userID=="Discord_ID" && tokenKeyName.toUpperCase() != "WAGMI" ){
                    return message.channel.send("Sorry, you are not authorized to send tips for this token.");
                }

                let data = getJson(botSettings.usersById);
                
                if(Object.keys(data).includes(user_id)){
                    let toAddress = data[user_id];
                    
                    //--- SEND TOKEN -----------------------
                    //sendToken(tokenInfo.address, userAddress, amount); //token address, user address

                    // Use BigNumber
                    let weiAmount = amount*Math.pow(10,tokenInfo.decimals);

                    let total = web3.utils.toBN(weiAmount).toString(); //https://web3js.readthedocs.io/en/v1.3.0/web3-utils.html?highlight=toBN#tobn
                    //console.log("Amount: ", weiAmount);
                    //console.log("Total: ", weiAmount);

                    const MyContract = new web3.eth.Contract(minABI2, tokenInfo.address); //Get ERC20 Token contract instance
                
                    //gas: web3.utils.toHex(120000), //120000
                    /* PROMISE
                    MyContract.methods.transfer(toAddress, total)
                        .send({from: botSettings.botAddress})
                        .then(function(receipt){ // receipt can also be a new contract instance, when coming from a "contract.deploy({...}).send()"
                            console.log( `Tip sent by ${user}` , receipt.transactionHash);

                            if( receipt.status ){
                                msg = `:tada: <@!${user_id}> you has received a tip of **${amount} ${botPrefix.toUpperCase()}**`;
                            }else{
                                msg = `:grimacing: Sorry, tip has not been sent, try again.`;
                            }
                            message.channel.send(msg);
                        });
                    */

                    //https://web3js.readthedocs.io/en/v1.3.0/web3-eth-contract.html#id29
                    MyContract.methods.transfer(toAddress, total) 
                        .send({from: botSettings.botAddress})
                        .on('transactionHash', function(hash){
                            console.log( `Tip sent of ${botPrefix.toUpperCase()} TX:` , hash);
                            msg = `:tada: <@!${user_id}> you has received a tip of **${amount} ${botPrefix.toUpperCase()}**`;
                            message.channel.send(msg);
                        })
                        /*
                        .on('confirmation', function(confirmationNumber, receipt){
                            console.log("Confirmation Num:",confirmationNumber);
                            console.log("Receipt:",receipt.transactionHash);
                        })
                        .on('receipt', function(receipt){
                            console.log("Receipt:",receipt.transactionHash);
                        })
                        */
                        .on('error', function(error, receipt) { // If the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
                            console.log("Error sending tip:",error);
                            console.log("Receipt:",receipt);
                        });
                        
                } else {
                    return message.channel.send(":thinking: this user is not registered.");
                }
            
        }else{
            return message.channel.send(":smile: Sorry, only administrators can use the command *tip*.");
        }        
    }


// Rain on the online and registered users.
    if(message.content.toLowerCase().startsWith(botPrefix+"rain ")){
        if( userID=="370711244245565441" || //Anabel
            userID=="339843008323256322" || //Ruul 
            userID=="300412335275769856" || //Bitjohn
            userID=="362312433412341781" || //Kannon
            userID=="351489283166699520" || //Omar
            userID=="419467013728108544" || //Mello
            userID=="547477878804054028" || //JJJH82
            userID=="360276639147360266" || //madwillysg
            userID=="537600835966861314" || //Wedergarten
            userID=="746169184135675947" || //Stoice
            userID=="778048794625179690" || //Stefal EABN
            userID=="804032424215969822" || //Just a Seal x420
            userID=="394268010929455104" //Aseriousgogetta
            ){ 
                
                //token rain rights for user Just a Seal
                if( userID=="804032424215969822" && tokenKeyName.toUpperCase() != "X420" ){
                    return message.channel.send( _lang(`:grimacing: I'm sorry, only the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Sorry, just the admin of **${tokenKeyName.toUpperCase()}** is authorized to make rains.`, serverId) );
                }

                //token rain rights for user Stefal
                if( userID=="778048794625179690"){
                    if(tokenKeyName.toUpperCase() == "EABN" || tokenKeyName.toUpperCase() == "EVLTZ"){
                        //let's go
                    }else{
                        return message.channel.send( _lang(`:grimacing: I'm sorry, only the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Sorry, just the admin of **${tokenKeyName.toUpperCase()}** is authorized to make rains.`, serverId) );
                    }
                }
                
                //token rain rights for user Stoice
                if( userID=="746169184135675947" && tokenKeyName.toUpperCase() != "ALYM" ){
                    return message.channel.send( _lang(`:grimacing: I'm sorry, only the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Sorry, just the admin of **${tokenKeyName.toUpperCase()}** is authorized to make rains.`, serverId) );
                }

                //token rain rights for user Wedergarten
                if( userID=="537600835966861314" && tokenKeyName.toUpperCase() != "ALYM" ){
                    return message.channel.send( _lang(`:grimacing: I'm sorry, only the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Sorry, just the admin of **${tokenKeyName.toUpperCase()}** is authorized to make rains.`, serverId) );
                }

                //token rain rights for user Omar
                if( userID=="351489283166699520" && tokenKeyName.toUpperCase() != "STK" ){
                    return message.channel.send( _lang(`:grimacing: I'm sorry, only the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Sorry, just the admin of **${tokenKeyName.toUpperCase()}** is authorized to make rains.`, serverId) );
                }

                //token rain rights for user Mello
                if( userID=="419467013728108544" && tokenKeyName.toUpperCase() != "HY511" ){
                    return message.channel.send( _lang(`:grimacing: I'm sorry, only the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Sorry, just the admin of **${tokenKeyName.toUpperCase()}** is authorized to make rains.`, serverId) );
                }

                //token rain rights for user JJJH82
                if( userID=="547477878804054028" && tokenKeyName.toUpperCase() != "SVIT" ){
                    return message.channel.send( _lang(`:grimacing: I'm sorry, only the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Sorry, just the admin of **${tokenKeyName.toUpperCase()}** is authorized to make rains.`, serverId) );
                }

                //token rain rights for user madwillysg
                if( userID=="360276639147360266" && tokenKeyName.toUpperCase() != "NST" ){
                    return message.channel.send( _lang(`:grimacing: I'm sorry, only the token admin can make rains for **${tokenKeyName.toUpperCase()}**`, `:grimacing: Sorry, just the admin of **${tokenKeyName.toUpperCase()}** is authorized to make rains.`, serverId) );
                }

                let amount = Number(args[1]);
                if (!amount){
                    return message.channel.send(`:thinking: You have entered an invalid amount of ${botPrefix.toUpperCase()} try again.`);
                } 

                // main raining func
                raining(amount, message, tokenKeyName, userName, serverId);
                
        }else{ 
            return message.channel.send( _lang(`:smile: I'm sorry, only admins can make rains.`, 
                                               `:smile: Sorry, only admins can make rains.`, serverId) );
        }
    }

    // Rain on the online and registered users.
    if(message.content.toLowerCase().startsWith(botPrefix+"rain.online ")){
        
        let amount = Number(args[1]);
        if (!amount){
            return message.channel.send(`:thinking: You have entered an invalid amount of ${botPrefix.toUpperCase()} try again.`);
        } 

        // main raining func
        rainingOnline(amount, message, tokenKeyName, userName);
    }

//-----------------------
// HELP Commands
//-----------------------

//-- Display Token info and contracts
    if(message.content.toLowerCase() === botPrefix+"getlist" || 
        message.content.toLowerCase() === botPrefix+"tokenlist"){

        //console.log(botSettings.tokens);
        let obj = botSettings.tokens;
        let tokenData = "";

        Object.keys(obj).forEach(key=>{
            //console.log(`${key} : ${obj[key]["address"]}`);
            if(key!="WEXP"){
                tokenData = `${tokenData}**${key}** (${obj[key]["name"]})\nContract address: \`\`\`${obj[key]["address"]}\`\`\`\n`; 
                //tokenData = `${tokenData}${key} (${obj[key]["name"]})\nContract address: ${obj[key]["address"]}\n\n`; 
            }
        });

        message.channel.send(`${tokenData}`);
    }

//-- Display Token info and contracts
    if(message.content.toLowerCase() === botPrefix+"info" || 
        message.content.toLowerCase() === botPrefix+"token"){

        const tokenInfo = botSettings.tokens[tokenKeyName];
        //console.log(tokenKeyName,tokenInfo);
        message.channel.send(
            `**${tokenKeyName}** (${tokenInfo.name})\n**Decimals:** ${tokenInfo.decimals}\n**Total supply:** ${tokenInfo.supply}\n**Contract address:**`
        );
        message.channel.send(
            `\`\`\`css\n${tokenInfo.address}\`\`\``
        );
        
        /*
        //console.log(botSettings.tokens);
        let obj = botSettings.tokens;
        let tokenData = "";
        Object.keys(obj).forEach(key=>{
            //console.log(`${key} : ${obj[key]["address"]}`);
            if(key!="WEXP"){
                tokenData = `${tokenData} **${key}** (${obj[key]["name"]}) \n **Contract address:** ${obj[key]["address"]} \n **Decimals:** ${obj[key]["decimals"]} \n **Total supply:** ${obj[key]["supply"]} \n\n`; 
            }
        });
        message.channel.send(tokenData);
        */
    }

//-- Display commands list
    if(message.content.toLowerCase() === botPrefix+"help" || 
       message.content.toLowerCase() === botPrefix+"help"){
        message.channel.send(":robot: Commands to use on PRM Bot:\n\n"+
            "**"+botPrefix+"register** *<address>* -  Register your EXP address, same address for all tokens. \n"+
            "**"+botPrefix+"bal** *<address>* -  Show ur token balances. \n"+ 
            "**"+botPrefix+"changeadd** *<address>* -  Update ur EXP address.\n"+ 
            "**"+botPrefix+"info** -  Token info.\n"+
            "**"+botPrefix+"rain** *<amount>* -  send tokens to all registered users (just Admins).\n"+ 
            "**"+botPrefix+"tip** *<user>* *<amount>* -  send tokens to the specified user (just Admins)\n\n"+
            "**"+botPrefix+"getlist** -  shows list of available tokens in the bot.\n"+
            "**"+botPrefix+"getinfo** -  shows bot information, address and balances of each token.\n"+
            "**"+botPrefix+"getaddr** -  shows the EXP bot address used for all tokens. \n" +
            "**"+botPrefix+"getbal** -  shows the balance of the token in the bot's wallet. \n" + 
            "**"+botPrefix+"getid** -  show user ID.\n\n"+
            "```**NOTA: The Promineros bot dsnt save your coins or tokens, everything is sent directly to your wallet.**```" 
        );
    }


//-----------------------
// COMMANDS FOR THE BOT
//-----------------------

//-- Display bot info
    if(message.content.toLowerCase() === botPrefix+"getinfo"){
    message.channel.send(
        `:robot: Info of the current BOT: \n **Prefijo:** ${botPrefix.toUpperCase()} \n **Bot address: ** ${botSettings.botAddress} \n **Balances** https://expexplorer.guarda.co/address/${botSettings.botAddress}`
    );
    }

//-- Display bot wallet
    if(message.content.toLowerCase() === botPrefix+"getaddr" ||
       message.content.toLowerCase() === botPrefix+"getaddress"){
        message.channel.send(
            `:robot: The address is: **${botSettings.botAddress}** \n If you want you can donate tokens or Expanse for the community, thank you!`
        );
    }

//-- Get discord user id.
    if(message.content.toLowerCase().startsWith(botPrefix+"getid")){
        //console.log(args[1]); <@!300412335275769856>
        if(args[1]){
            let user_id = args[1].replace(/\D/g, ""); //extract ID user
            return message.channel.send(`${args[1]} your Discord ID is: ${user_id}`);    
        }else{
            return message.channel.send(`<@!${userID}> your Discord ID is: ${userID}`);    
        }
    }

//-- Get BOT token balances
    if(message.content.toLowerCase() === botPrefix+"getbal" || 
        message.content.toLowerCase() === botPrefix+"getbalance"){

        //Get current token info
        const tokenInfo = botSettings.tokens[tokenKeyName];
        //console.log("Token Info:", tokenInfo);
        //console.log("Obj: ",tokenInfo);

        //const data = getJson(botSettings.usersById);
        const walletAddress = botSettings.botAddress;

        if(tokenInfo === undefined){
            return message.channel.send("Sorry, the token or currency is not available in our bot yet.");
        }

        if(walletAddress === undefined){
            return message.channel.send("There is no wallet registered for this bot.");
        }

        //Get Token balance            
        //Get ERC20 Token contract instance
        //const contract = new web3.eth.Contract(minABI, tokenAddress, {from:walletAddress});
        const MyContract = new web3.eth.Contract(minABI, tokenInfo.address);

        // Call balanceOf function
        MyContract.methods.balanceOf(walletAddress).call()
            .then(function(balance){
                //console.log( "Total PRM:", (balance/Math.pow(10,18)).toFixed(3) );                
                balance = (balance/Math.pow(10,tokenInfo.decimals)).toFixed(8);

                message.channel.send( 
                    _lang( `:robot: Bot balance is **${balance} ${botPrefix.toUpperCase()}**, would be nice if you donate me some tokens to share with this amazing community, type command **${botPrefix}getaddress** to display my wallet.`, 
                           `:robot: El bot tiene **${balance} ${botPrefix.toUpperCase()}**, would be nice if you donate me some tokens to share with this amazing community, type command **${botPrefix}getaddress** to see my wallet address.`, 
                           serverId ) 
                );
            });
    }



});





//-- FUNCTIONS --

function getJson(path){
	return JSON.parse(fs.readFileSync(path));
}


function sendToken(tokenAddress, toAddress, amount){    
    // Use BigNumber
    let weiAmount = amount*Math.pow(10,18);
    let total = web3.utils.toBN(weiAmount).toString(); //https://web3js.readthedocs.io/en/v1.3.0/web3-utils.html?highlight=toBN#tobn

    //console.log("Amount: ", weiAmount);
    //console.log("Total: ", weiAmount);

    //Get ERC20 Token contract instance
    //const contract = new web3.eth.Contract(minABI, tokenAddress, {from:walletAddress});
    const MyContract = new web3.eth.Contract(minABI2, tokenAddress);
  
    //gas: web3.utils.toHex(120000), //120000
    MyContract.methods.transfer(toAddress, total)
        .send({from: '0xD4dbE54c51445E13ea0aa43c13A3b23BA5613baa'})
        .then(function(receipt){
            // receipt can also be a new contract instance, when coming from a "contract.deploy({...}).send()"
            console.log("RESULT: ", receipt);
        });
}

// Main sending function.
function sendCoins(address,value,message,userId){
/*
    console.log("sent:"+ value);	
	console.log("fixed:"+ value.toFixed(8) );	
	console.log("toString:"+ numberToString(value) );	

	web3.eth.sendTransaction({
	    from: botSettings.address,
	    to: address,
	    gas: web3.utils.toHex(120000), //120000
	    value: numberToString(value) //(value).toFixed(8) //
	})
	.on('transactionHash', function(hash){
		// sent pm with their tx
		// recive latest array
		if(userId != 1){
			let fValue = value/Math.pow(10,18).toFixed(8);
			//let username = bot.users.find('username',name);
			let author = bot.users.find('id',userId);
			//author.send("Hi "+name+" , you are lucky man.\n Check hash: https://explorer.expanse.tech/tx/"+ hash);
			
			//author.send("Hey **"+getUsernameById(userId)+"**, u are lucky, has received a rain of **"+fValue+" EXP**.\n :moneybag: Look the transaction on: https://explorer.expanse.tech/tx/"+ hash + " \n \n **Get the latest news and updates on EXPANSE** https://expanse.tech/expanse-newsletter-vol-4-no-17-09-30-2019/");
			author.send("Hey **"+getUsernameById(userId)+"**, u are lucky, has received a rain of **"+fValue+" EXP**.\n :moneybag: Look the transaction on: https://explorer.expanse.tech/tx/"+ hash);
			author.send("Have you tried the new Expanse faucet for Android? Complete 5000 XP and claim EXP directly on your wallet. https://play.google.com/store/apps/details?id=com.expfaucet");
			//author.send("Invite your friends or family so that the ProMineros team carry out more rains and raffles. https://cdn.discordapp.com/attachments/527515942251659266/629676145813618689/lluvia-de-invitados.jpg");
			//author.send("https://media.discordapp.net/attachments/527515942251659266/632584848275537920/dia-lluvioso.jpg");
		} else {
			message.channel.send(":moneybag: tip sent! Look the transaction on: https://explorer.expanse.tech/tx/"+ hash); //"Tip was sent. \n Check hash: https://explorer.expanse.tech/tx/"+ hash
		}
	})
    .on('error', console.error);
    */
}


// Raining command to send users coin.
function raining(amount, message, tokenKeyName, userName, serverId){
    const onlineUsers = getOnline(); // online users
	const data = getJson(botSettings.usersById); // registered users

	// Create online and register array user ids
	//var onlineAndRegister = Object.keys(data).filter(username => {return onlineUsers.indexOf(username)!=-1});
	let onlineAndRegister = Object.keys(data).filter(id => {return onlineUsers.indexOf(id)!=-1});    
    
    // create object with name - address and name - values
	let latest = {};
	for (let theUser of onlineAndRegister) {
	  if (data[theUser]) {
	    latest[data[theUser]] = theUser;
	  }
	}

	//if(Object.keys(onlineUsers).length > 0){
        //Get current token info
        let tokenInfo = botSettings.tokens[tokenKeyName];
        console.log("Token Info:", tokenInfo);

		// if use wrong amount (string or something)
		let camount = amount/Object.keys(latest).length;
        let weiAmount = 0;

        if(tokenKeyName.toUpperCase()=="EABN"){
            camount = camount.toFixed(6);
            weiAmount = camount*Math.pow(10,tokenInfo.decimals).toFixed(6);
        }else{
            camount = camount.toFixed(8);
            weiAmount = camount*Math.pow(10,tokenInfo.decimals).toFixed(8);
        }

        let theGasLimit = 210000;
        if(tokenKeyName.toUpperCase()=="EVLTZ"){
            theGasLimit = 3000000;
        }

        let total = web3.utils.toBN(weiAmount).toString(); //https://web3js.readthedocs.io/en/v1.3.0/web3-utils.html?highlight=toBN#tobn

        const MyContract = new web3.eth.Contract(minABI2, tokenInfo.address); //Get ERC20 Token contract instance

		//message.channel.send( `:thunder_cloud_rain: Has been launched a **rain** of ${tokenKeyName}'s to **${Object.keys(latest).length}** online users!. check ur DM to verify it.` ); 
        console.log(`Rain thrown by: ${userName} of ${amount} ${tokenKeyName}` );
        
        //How many users
        let sentUsers = "";
        let sentUsers2 = "";
        let i = 0;

        for(const toAddress of Object.keys(latest)){
            let user_id = latest[toAddress];
            
            //sendCoins(address, weiAmount, message, user_id);
            if(i<=80){
                sentUsers += `<@!${user_id}> `;
            }else{
                sentUsers2 += `<@!${user_id}> `;
            }
            i++;

            //https://web3js.readthedocs.io/en/v1.3.0/web3-eth-contract.html#id29
            MyContract.methods.transfer(toAddress, total) 
                .send({from: botSettings.botAddress, gas: theGasLimit})
                .on('transactionHash', function(hash){
                    console.log( `Rain sent of ${tokenKeyName} TX:`, hash);
                    //msg = `:tada: <@!${user_id}> you has received a tip of **${amount} ${botPrefix.toUpperCase()}**`;
                    //message.channel.send(msg);
                })
                //.on('confirmation', function(confirmationNumber, receipt){
                //    console.log("Confirmation Num:",confirmationNumber);
                //    console.log("Receipt:",receipt.transactionHash);
                //})
                //.on('receipt', function(receipt){
                //    console.log("Receipt:",receipt.transactionHash);
                //})
                .on('error', function(error, receipt) { // If the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
                    console.log("Rain has been failed:",error);
                    console.log("Receipt:",receipt);
                });
        }

        console.log("Rain sent:", sentUsers);
        console.log("Rain each:", weiAmount);
        message.channel.send(
            _lang(`:thunder_cloud_rain: It's raining **${amount} ${tokenKeyName}** on users: ${sentUsers}, each one has received **${camount} ${tokenKeyName}**`,
                  `:thunder_cloud_rain: It's raining **${amount} ${tokenKeyName}** on users: ${sentUsers}, each one has received **${camount} ${tokenKeyName}**`,
                  serverId) );
        if(sentUsers2){
            message.channel.send(
                _lang(`:thunder_cloud_rain: ${sentUsers2}, each one has received **${camount} ${tokenKeyName}**`,
                      `:thunder_cloud_rain: ${sentUsers2}, each one has received **${camount} ${tokenKeyName}**`,
                      serverId) );
        }

	//}else{
	//	message.channel.send(":thinking: ups! no users online.");
	//}
}


// RainingOnline command to send users coin.
function rainingOnline(amount, message, tokenKeyName, userName){
    const onlineUsers = getOnline(); // online users
	const data = getJson(botSettings.usersById); // registered users

	// Create online and register array user ids
	//var onlineAndRegister = Object.keys(data).filter(username => {return onlineUsers.indexOf(username)!=-1});
	let onlineAndRegister = Object.keys(data).filter(id => {return onlineUsers.indexOf(id)!=-1});    
    
    // create object with name - address and name - values
	let latest = {};
	for (let theUser of onlineAndRegister) {
	  if (data[theUser]) {
	    latest[data[theUser]] = theUser;
	  }
	}
/*
	//if(Object.keys(onlineUsers).length > 0){
        //Get current token info
        let tokenInfo = botSettings.tokens[tokenKeyName];
        console.log("Token Info:", tokenInfo);

		// if use wrong amount (string or something)
		let camount = amount/Object.keys(latest).length;
        camount = camount.toFixed(8);
        let weiAmount = camount*Math.pow(10,tokenInfo.decimals).toFixed(8);

        let total = web3.utils.toBN(weiAmount).toString(); //https://web3js.readthedocs.io/en/v1.3.0/web3-utils.html?highlight=toBN#tobn

        const MyContract = new web3.eth.Contract(minABI2, tokenInfo.address); //Get ERC20 Token contract instance

		//message.channel.send( `:thunder_cloud_rain: Has been launched a **rain** of ${tokenKeyName}'s to **${Object.keys(latest).length}** online users!. check ur DM to verify it.` ); 
        console.log(`rain thrown by: ${userName} of ${amount} ${tokenKeyName}` );
        
        //How many users
        let sentUsers = "";
        let sentUsers2 = "";
        let i = 0;

        for(const toAddress of Object.keys(latest)){
            let user_id = latest[toAddress];
            
            //sendCoins(address, weiAmount, message, user_id);
            if(i<=80){
                sentUsers += `<@!${user_id}> `;
            }else{
                sentUsers2 += `<@!${user_id}> `;
            }
            i++;

            //https://web3js.readthedocs.io/en/v1.3.0/web3-eth-contract.html#id29
            MyContract.methods.transfer(toAddress, total) 
                .send({from: botSettings.botAddress})
                .on('transactionHash', function(hash){
                    console.log( `Rain sent of ${tokenKeyName} TX:`, hash);
                    //msg = `:tada: <@!${user_id}> you has received a tip of **${amount} ${botPrefix.toUpperCase()}**`;
                    //message.channel.send(msg);
                })
                //.on('confirmation', function(confirmationNumber, receipt){
                //    console.log("Confirmation Num:",confirmationNumber);
                //    console.log("Receipt:",receipt.transactionHash);
                //})
                //.on('receipt', function(receipt){
                //    console.log("Receipt:",receipt.transactionHash);
                //})
                .on('error', function(error, receipt) { // If the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
                    console.log("Rain has been failed:",error);
                    console.log("Receipt:",receipt);
                });
        }

        console.log("Rain sent:", sentUsers);
        console.log("Rain each:", weiAmount);
        message.channel.send(`:thunder_cloud_rain: Promineros has launched a rain of **${amount} ${tokenKeyName}**, for the users: ${sentUsers}, each one has received **${camount} ${tokenKeyName}**`);
        if(sentUsers2){
            message.channel.send(`:thunder_cloud_rain: Promineros has launched a rain of **${amount} ${tokenKeyName}**, for the users: ${sentUsers2}, each one has received **${camount} ${tokenKeyName}**`);
        }

	//}else{
	//	message.channel.send(":thinking: ups! no users online.");
	//}
*/
}

// return array with names of online users
function getOnline(){
    const data = getJson(botSettings.usersById); // registered users
    let result = [];

    //data.forEach( aUsers =>{
    //    console.log(aUsers);
    //});

    for (let key in data){
        if(data.hasOwnProperty(key)){
          //console.log(`${key} : ${data[key]}`)
          result.push(key);
        }
    }

    //console.log(result);
    return result;

    /*
    //https://stackoverflow.com/questions/64349914/get-guild-members-and-filter-them-discord-js
    let guild = bot.guilds.cache.get('537789773994131467'); //527515942251659264 
    let userCount = guild.memberCount;
    //let onlineCount = guild.members.cache.filter(member => member.presence.status === 'online').size;
    let foo = [];

    //https://discord.js.org/#/docs/main/12.5.1/class/GuildMemberManager
    //guild.members.fetch({force:true})
    //    .then(console.log)
    //    .catch(console.error);
                
    // Fetch by an array of users including their presences
    // TO DO: falta que presence.status retorne los usuario online //, 
    guild.members.fetch({ user: ['300412335275769856', '419467013728108544', '408912102443450368'], withPresences: true, cache: false, force: true })
        .then(rUsers => {
            //console.log("MEMBER:",m)
            rUsers.forEach((rUser) => {
                console.log("M ID", rUser.id);
                console.log("M PRESENSE", rUser.presence.status);
            })
        })
        .catch(console.error);
    */
    /*
    //console.log("MEMBER CACHE",guild.members.cache);
    guild.members.cache.forEach((member) => {
        //console.log("VAL ID",member.id);
        //console.log("VAL PRESENSE",member.presence.status);
        console.log("M STATUS:",member.presence.status);
		if(member.presence.status === "online"){
			foo.push(member.id);
		}
    });
    console.log(foo);
    */

    //console.log("userCount:",userCount);
    //console.log("onlineCount",onlineCount);
    //return;
}

/*
function getOnline(){
	let foo = [];
    //let users = bot.users;
    let guilds = bot;

    console.log("El bot: ",guilds);
    return;

	users.keyArray().forEach((val) => {
		let userName = users.get(val).id;
		let status = users.get(val).presence.status;
		if(status == "online"){
			foo.push(userName);
		}
    });
    
	return foo;
}
*/

function _lang(en, es, serverId){
    //527515942251659264  PRM Server ID
    //351390305561346060 EXP Server ID
    //
    let result = en;

    if(serverId=="527515942251659264"){
        result = es;
    }
    
    return result;
}

bot.login(botSettings.discordToken);
