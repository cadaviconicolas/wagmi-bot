// Add bot to discord server:
// https://discord.com/oauth2/authorize?client_id=795347075000434718&scope=bot&permissions=8
// Tuto: https://www.digitaltrends.com/gaming/how-to-make-a-discord-bot/
// Tuto: https://medium.com/davao-js/tutorial-creating-a-simple-discord-bot-9465a2764dc0
// node index.js

//Send tx without having to unlock the account 
//https://stackoverflow.com/questions/46611117/how-to-authenticate-and-send-contract-method-using-web3-js-1-0

// Get ERC20 Token Balance
// https://medium.com/@piyopiyo/how-to-get-erc20-token-balance-with-web3-js-206df52f2561
// https://piyolab.github.io/playground/ethereum/getERC20TokenBalance/

// Send Send ERC20 Token
// https://piyolab.github.io/playground/ethereum/sendERC20Token/

const Web3          = require('web3');
const Discord       = require('discord.io');
const logger        = require('winston');
//const BigNumber     = require('bignumber.js');
const botSettings   = require('./botSettings.json');
const { add }       = require('winston');
const fs            = require("fs");

const minABI = [
    // balanceOf
    {
        "constant":true,
        "inputs":[{"name":"_owner","type":"address"}],
        "name":"balanceOf",
        "outputs":[{"name":"balance","type":"uint256"}],
        "type":"function"
    },
    // decimals
    {
        "constant":true,
        "inputs":[],
        "name":"decimals",
        "outputs":[{"name":"","type":"uint8"}],
        "type":"function"
}];

const minABI2 = [
    // transfer
    {
      "constant": false,
      "inputs": [
        {
          "name": "_to",
          "type": "address"
        },
        {
          "name": "_value",
          "type": "uint256"
        }
      ],
      "name": "transfer",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "type": "function"
    }
];

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';

// Initialize web3
const web3 = new Web3(Web3.givenProvider || botSettings.web3provider);

// Initialize Discord Bot
const bot = new Discord.Client({
   token: botSettings.discordToken,
   autorun: true
});
//const bot = new Discord.Client({disableEveryone:true});
//bot.login(botSettings.discordToken);

bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
    console.log(bot.username + ", inicializado.");
});

bot.on('message', function (user, userID, channelID, message, evt) {
	const args = message.split(' ');

    //Get current prefix
    const prefix = message.split('.'); //console.log("Prefix Actual: "+prefix[0]);
    const botPrefix = prefix[0].toLowerCase()+"."; //asign dinamic prefix
    let tokenKeyName = prefix[0].toUpperCase();
    //console.log("Token Name:", tokenKeyName);

    /*
    for (var key in botSettings.tokens) {
        if(key == tokenName){
            console.log(key);
            console.log(botSettings.tokens[key]);
        }
    }*/


//-- Register user by discord ID
    if(message.toLowerCase().startsWith(botPrefix+"register ")){
        const address = args[1];
        let msg = "";

        if(web3.utils.isAddress(args[1])){	
            let data = getJson(botSettings.usersById);
            //console.log("add: "+ Object.values(data).includes(address));
            //console.log("userID: "+ Object.keys(data).includes(userID));
            if(!Object.values(data).includes(address) && !Object.keys(data).includes(userID)){		
                data[userID] = address;
                msg = ":tada: **@"+user+"** Has been registered successfully ur EXP address: " + address; //
                
                fs.writeFile(botSettings.usersById, JSON.stringify(data), (err) => {
                    if (err) throw err;
                        console.log( `A new user has registered your wallet: ${user}` ); //The file has been saved.
                    });	
                
            } else {
                msg = ":thumbsup: **@"+user+"** U have already registered your wallet, to update it use the command **"+botPrefix+"changeadd**";
            }
        } else {
            msg = ":open_mouth: **@"+user+"** U trying to register an invalid EXP address. Please try again and make sure it has the following format: **"+botPrefix+"register 0xAddress...**";
        }
        
        bot.sendMessage({
            to: channelID,
            message: msg
        });
    }


//-- Change registration with bot.
    if(message.toLowerCase().startsWith(botPrefix+"changeadd")){
        const address = args[1];
        let msg = "";
        
        if(web3.utils.isAddress(args[1])){
            let data = getJson(botSettings.usersById);
            if(Object.keys(data).includes(userID)){
                if(address != data[userID]){
                    data[userID] = address;
                    fs.writeFile(botSettings.usersById, JSON.stringify(data), (err) => {
                        if (err) throw err;
                            console.log('User '+user+' wallet has been changed.'); //The file has been changed
                        });
                    msg = ":thumbsup: **@"+user+"** Your old EXP address has been changed to: "+address;
                } else {
                    msg = "**@"+user+"** Seems that you already have this address registered, use a different one if you are trying to change the previous one.";
                }
            } else {
                msg = ":open_mouth: You are not in the list, first register your EXP address with the command **"+botPrefix+"register** *<address>*"; 
            }
        } else {
            msg = ":open_mouth: **@"+user+"** you are trying to register a wrong EXP address, try again with the format: **"+botPrefix+"register 0xAddress**"; 
        }

        bot.sendMessage({
            to: channelID,
            message: msg
        });
    }

//-- Check to see user wallet.
    if(message.toLowerCase() == botPrefix+"myaddress"){
        const data = getJson(botSettings.usersById);
        const wallet = data[userID];
        let msg = "";

        if(Object.keys(data).includes(userID)){
            msg = ":pouch: **@"+user+"** your EXP wallet is: "+wallet; //already registered
        } else {
            msg = ":open_mouth: **@"+user+"** you are not registered, use the command **"+botPrefix+"register** *<address>*";
        }

        bot.sendMessage({
            to: channelID,
            message: msg
        });
    }

//-- Get token balance
    if(message.toLowerCase() === botPrefix+"bal" || message.toLowerCase() === botPrefix+"balance"){
        const data = getJson(botSettings.usersById);
        const walletAddress = data[userID];

        //Get current token info
        let tokenInfo = botSettings.tokens[tokenKeyName];
        //console.log("Token Info:", tokenInfo);
        //console.log("Obj: ",tokenInfo);

        if(tokenInfo === undefined){
            bot.sendMessage({
                to: channelID,
                message: "Sorry, the token or currency is not available in our bot yet."
            });
            return;
        }

        //Get Token balance            
        //Get ERC20 Token contract instance
        //const contract = new web3.eth.Contract(minABI, tokenAddress, {from:walletAddress});
        const MyContract = new web3.eth.Contract(minABI, tokenInfo.address);
        
        // Call balanceOf function
        MyContract.methods.balanceOf(walletAddress).call()
            .then(function(balance){
                //console.log( "Total PRM:", (balance/Math.pow(10,18)).toFixed(3) );
                balance = (balance/Math.pow(10,18)).toFixed(3);
                const msg = `:moneybag: **${user}** u have **${balance} ${botPrefix.toUpperCase()}**`;
                bot.sendMessage({
                    to: channelID,
                    message: msg
                });
            });
    }

//-- Send Tip a user.
if(message.toLowerCase().startsWith(botPrefix+"test ")){
    //console.log("Tip:" + userID);
    console.log("Debug: ", args[1]);


}


//-- Send Tip a user.
    if(message.toLowerCase().startsWith(botPrefix+"tip ")){
        //console.log("Tip:" + userID);
        let msg = "";

        if( userID=="370711244245565441" || //Anabel
            userID=="339843008323256322" || //Ruul 
            userID=="300412335275769856" || //Bitjohn
            userID=="419467013728108544" ){ //Mello
                let user_id = args[1];
                let amount = Number(args[2]);

                //Get current token info
                let tokenInfo = botSettings.tokens[tokenKeyName];
                //console.log("Token Info:", tokenInfo);
                
                if(tokenInfo === undefined){
                    bot.sendMessage({
                        to: channelID,
                        message: "Sorry, the token or currency is not available in our bot yet."
                    });
                    return;
                }

                if (!amount){
                    // if use wrong amount (string or something)
                    bot.sendMessage({
                        to: channelID,
                        message: `:thinking: You have entered an invalid amount of ${tokenKeyName}, try again.`
                    });
                    return;
                }

                
                if( userID=="419467013728108544" ){
                    if( tokenKeyName.toUpperCase() == "PRM" || tokenKeyName.toUpperCase() == "LOVE" ){
                        bot.sendMessage({
                            to: channelID,
                            message: "Sorry, you are not authorized to send tips for this token."
                        });
                        return;
                    }

                    /*
                    if( tokenKeyName.toUpperCase() == "HY511" || tokenKeyName.toUpperCase() == "EGG" ){
                        //GO Tip!
                    }else{
                        bot.sendMessage({
                            to: channelID,
                            message: "Sorry, you are not authorized to send tips for this token."
                        });
                        return;
                    }*/

                }
                
                let data = getJson(botSettings.usersById);
                
                if(Object.keys(data).includes(user_id)){
                    let toAddress = data[user_id];
                    
                    //--- SEND TOKEN -----------------------
                    //sendToken(tokenInfo.address, userAddress, amount); //token address, user address

                    // Use BigNumber
                    let weiAmount = amount*Math.pow(10,18);
                    let total = web3.utils.toBN(weiAmount).toString(); //https://web3js.readthedocs.io/en/v1.3.0/web3-utils.html?highlight=toBN#tobn
                    //console.log("Amount: ", weiAmount);
                    //console.log("Total: ", weiAmount);

                    const MyContract = new web3.eth.Contract(minABI2, tokenInfo.address); //Get ERC20 Token contract instance
                
                    //gas: web3.utils.toHex(120000), //120000
                    MyContract.methods.transfer(toAddress, total)
                        .send({from: botSettings.botAddress})
                        .then(function(receipt){ // receipt can also be a new contract instance, when coming from a "contract.deploy({...}).send()"
                            console.log( `Tip sent from ${user}` , receipt.transactionHash);

                            if( receipt.status ){
                                msg = `:tada: <@${user_id}> you has received a tip of **${amount} ${botPrefix.toUpperCase()}**`;
                            }else{
                                msg = `:grimacing: sorry, the tip has not been sent, try again.`;
                            }

                            bot.sendMessage({
                                to: channelID,
                                message: msg
                            });
                        });

                } else {
                    bot.sendMessage({
                        to: channelID,
                        message: ":thinking: this user is not registered."
                    });
                }
            
        }else{
            bot.sendMessage({
                to: channelID,
                message: ":smile: Sorry, only administrators can use the command *tip*."
            });
        }

        
    }


// Rain on the online and registered users.
    if(message.toLowerCase().startsWith(botPrefix+"rain")){
        //userID=="351489283166699520" || //Omar
        if( userID=="370711244245565441" || //Anabel
            userID=="339843008323256322" || //Ruul 
            userID=="300412335275769856" || //Bitjohn
            userID=="362312433412341781" ){ //Kannon
                    
                let amount = Number(args[1]);
                if (!amount){
                    bot.sendMessage({
                        to: channelID,
                        message: `:thinking: You have entered an invalid amount of ${botPrefix.toUpperCase()} try again.`
                    });
                    return; 
                } 

                // main func
                raining(amount, message);
                console.log("rain hecho por: "+user);
        }else{ 
            bot.sendMessage({
                to: channelID,
                message: ':smile: Sorry, only admins can make rains.'
            });
            return;
        }
    }


//-- Get discord user id.
    if(message.toLowerCase() === botPrefix+"getid"){
        //console.log(user);
        //console.log(userID);
        
        bot.sendMessage({
            to: channelID,
            message: `**@${user}**, your user ID is: ${userID}`
        });

    }


    /*
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`
    if (message.substring(0, 1) == '!') {
        let args = message.substring(1).split(' ');
        const cmd = args[0];
        args = args.splice(1);
        switch(cmd) {
            // !ping
            case 'ping':
                bot.sendMessage({
                    to: channelID,
                    message: 'Pong!'
                });
            break;
            // Just add any case commands if you want to..
         }
     }
     */

});




//-- FUNCTIONS --

function getJson(path){
	return JSON.parse(fs.readFileSync(path));
}


function sendToken(tokenAddress, toAddress, amount){    
    // Use BigNumber
    let weiAmount = amount*Math.pow(10,18);
    let total = web3.utils.toBN(weiAmount).toString(); //https://web3js.readthedocs.io/en/v1.3.0/web3-utils.html?highlight=toBN#tobn

    //console.log("Amount: ", weiAmount);
    //console.log("Total: ", weiAmount);

    //Get ERC20 Token contract instance
    //const contract = new web3.eth.Contract(minABI, tokenAddress, {from:walletAddress});
    const MyContract = new web3.eth.Contract(minABI2, tokenAddress);
  
    //gas: web3.utils.toHex(120000), //120000
    MyContract.methods.transfer(toAddress, total)
        .send({from: '0xD4dbE54c51445E13ea0aa43c13A3b23BA5613baa'})
        .then(function(receipt){
            // receipt can also be a new contract instance, when coming from a "contract.deploy({...}).send()"
            console.log("RESULT: ", receipt);
        });
}

// Main sending function.
function sendCoins(address,value,message,userId){
/*
    console.log("enviado:"+ value);	
	console.log("fixed:"+ value.toFixed(8) );	
	console.log("toString:"+ numberToString(value) );	

	web3.eth.sendTransaction({
	    from: botSettings.address,
	    to: address,
	    gas: web3.utils.toHex(120000), //120000
	    value: numberToString(value) //(value).toFixed(8) //
	})
	.on('transactionHash', function(hash){
		// sent pm with their tx
		// recive latest array
		if(userId != 1){
			let fValue = value/Math.pow(10,18).toFixed(8);
			//let username = bot.users.find('username',name);
			let author = bot.users.find('id',userId);
			//author.send("Hi "+name+" , you are lucky man.\n Check hash: https://explorer.expanse.tech/tx/"+ hash);
			
			//author.send("Hola **"+getUsernameById(userId)+"**, u are lucky, has received a rain of **"+fValue+" EXP**.\n :moneybag: Look the transaction on: https://explorer.expanse.tech/tx/"+ hash + " \n \n **Conoce la últimas noticas y actualizaciones en EXPANSE** https://expanse.tech/expanse-newsletter-vol-4-no-17-09-30-2019/");
			author.send("Hola **"+getUsernameById(userId)+"**, u are lucky, has received a rain of **"+fValue+" EXP**.\n :moneybag: Look the transaction on: https://explorer.expanse.tech/tx/"+ hash);
			author.send("Have you tried the new Expanse faucet for Android? Complete 5000 XP and claim EXP directly on your wallet. https://play.google.com/store/apps/details?id=com.expfaucet");
			//author.send("Invite your friends or family so that the ProMineros team carry out more rains and raffles. https://cdn.discordapp.com/attachments/527515942251659266/629676145813618689/lluvia-de-invitados.jpg");
			//author.send("https://media.discordapp.net/attachments/527515942251659266/632584848275537920/dia-lluvioso.jpg");
		} else {
			message.channel.send(":moneybag: tip sent! Look the transaction on: https://explorer.expanse.tech/tx/"+ hash); //"Tip was sent. \n Check hash: https://explorer.expanse.tech/tx/"+ hash
		}
	})
    .on('error', console.error);
    */
}


// Raining command to send users coin.
function raining(amount,message){
	const data = getJson(botSettings.usersById); // registered users
    const onlineUsers = getOnline(); // online users
    
    console.log("Online Users:",onlineUsers);
    return;

	// create online and register array
	//var onlineAndRegister = Object.keys(data).filter(username => {return onlineUsers.indexOf(username)!=-1});
	let onlineAndRegister = Object.keys(data).filter(id => {return onlineUsers.indexOf(id)!=-1});
	// create object with name - address and name - values
	let latest = {};
	for (let user of onlineAndRegister) {
	  if (data[user]) {
	    latest[data[user]] = user;
	  }
	}

	//if(Object.keys(onlineUsers).length > 0){
		// if use wrong amount (string or something)
		var camount = amount/Object.keys(latest).length;
		var weiAmount = camount*Math.pow(10,18);

		message.channel.send(":thunder_cloud_rain: A **rain** of EXP's has fallen on **" + Object.keys(latest).length + "** online users!!. check ur DM to verify it." ); //"It just **rained** on **" + Object.keys(latest).length + "** users. Check DM." 

		function rainSend(addresses){
			for(const address of Object.keys(addresses)){
				let userId = addresses[address];
				sendCoins(address,weiAmount,message,userId);
			}
		}
		// main function
		rainSend(latest);	
	//}else{
	//	message.channel.send(":thinking: ups! no online users.");
	//}
}


// return array with names of online users
function getOnline(){
	let foo = [];
    //let users = bot.users;
    let guilds = bot;

    console.log("El bot: ",guilds);
    return;

	users.keyArray().forEach((val) => {
		let userName = users.get(val).id;
		let status = users.get(val).presence.status;
		if(status == "online"){
			foo.push(userName);
		}
    });
    
	return foo;
}
